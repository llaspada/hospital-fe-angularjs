export default route;

function route($routeProvider) {
  'ngInject';
  $routeProvider.otherwise({
    redirectTo: '/home'
  });
}
