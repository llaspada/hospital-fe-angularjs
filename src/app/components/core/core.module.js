import ngRoute from 'angular-route';
import ngResource from 'angular-resource';
import ngSanitize from 'angular-sanitize';
import _ from 'lodash';
import route from './core.route';

export default angular
  .module('app.core', [
    ngRoute,
    ngResource,
    ngSanitize
  ])
  .config(route)
  .constant('_', _)
  .name;
