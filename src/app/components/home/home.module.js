import HomeController from './home.controller';
import route from './home.route';

export default angular
  .module('app.home', [])
  .config(route)
  .controller('HomeController', HomeController)
  .name;
