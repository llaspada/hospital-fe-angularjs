import * as angular from 'angular';
import DummyTsController from './dummy-ts.controller';

export default angular
  .module('app.dummyTs', [])
  .controller('DummyTsController', DummyTsController)
  .name
