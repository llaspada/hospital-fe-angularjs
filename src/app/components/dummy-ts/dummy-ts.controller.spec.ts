let DummyTsController;

describe('Controller: DummyTsController', () => {
  beforeEach(() => {
    angular.mock.inject(($controller, $rootScope) => {
      DummyTsController = $controller('DummyTsController as vm', { $scope: $rootScope.$new() });
    });
  });

  it('should return 0 when multiplying by 0',() => {
    expect(DummyTsController.multiply(0, 2)).toEqual(0);
  });

  it('should return Infinity when dividing by 0', () => {
    expect(DummyTsController.divide(3, 0)).toEqual(Number.POSITIVE_INFINITY);
  });
});
