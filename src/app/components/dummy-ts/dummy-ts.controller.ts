export default class DummyTsController {
  constructor(private _: _.LoDashStatic) {
    'ngInject';
  }

  divide(x: number, y: number): number {
    return (this._ as any).divide(x, y);
  };

  multiply(x: number, y: number): number {
    return x * y;
  };
}
