export default class DummyEs6Controller {
  constructor(_) {
    'ngInject';

    this._ = _
  }

  divide(x, y) {
    return x / y;
  };

  multiply(x, y) {
    return this._.multiply(x, y);
  };
}
