import DummyEs6Controller from './dummy-es6.controller';

export default angular
  .module('app.dummyEs6', [])
  .controller('DummyEs6Controller', DummyEs6Controller)
  .name
